<h1>Coding Challenge</h1>

<h2>Introduction</h2>
<p>Hello, This is coding challenge for web development.</p>
<p>You will have the following task to acommplish:</p>

<ol>
    <li>HTML &amp; CSS</li>
    <li>Javascript and JQuery</li>
    <li>PHP and Codeigniter</li>
    <li>REST API</li>
</ol>
<p>You will need the following to accomplish the task:</p>
<ul>
    <li>BitBucket account</li>
    <li>XAMPP/LAMPP localhost enviroment installed on your computer</li>
</ul>
<h2>Instructions</h2>
<ol>
<li>You are to complete the task based on the above topics. Requirements will be given in each page of the task</li>
<li>You are to duplicate and clone the repository: <code>git clone https://daryllchu@bitbucket.org/userwebteam/coding-challenge.git</code></li>
<li>Perform the required task on your localhost</li>
<li>Commit and push your work onto the repository under the <strong>branch development</strong></li>
</ol>